﻿namespace _28._2
{
    class EuroConversie
    {
        private static decimal _koers = 40.3399M;

        public static decimal Koers
        {
            get => _koers;

        }



        public decimal ToBef(decimal amount)
        {
            return Koers * amount;
        }
        public decimal ToEuro(decimal amount)
        {
            return amount / Koers;
        }


    }


}

