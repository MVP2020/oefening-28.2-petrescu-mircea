﻿using System;

namespace _28._2
{
    class Rijksregisternummer
    {
        public bool IsValid(string rrnr)
        {
            int number;
            int rest;
            int numberToCheck;

            number = Convert.ToInt32(rrnr.Substring(0, 9));
            numberToCheck = Convert.ToInt32(rrnr.Substring(9));
            rest = number % 97;
            number = 97 - rest;
            if (number == numberToCheck)
            {
                return true;
            }
            else
            {
                return false;
            }

        }


    }
}
