﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace _28._2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Schrikkeljaar year = new Schrikkeljaar();
            lblSchrikkeljaar.Content = year.IsSchrikkeljaar(Convert.ToInt32(txtSchrikkeljaar.Text)).ToString();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            EuroConversie converttoday = new EuroConversie();
            lbConversie.Content = converttoday.ToEuro(Convert.ToInt32(txtConversie.Text)).ToString() + Environment.NewLine + converttoday.ToBef(Convert.ToInt32(txtConversie.Text));
        }

        private void btnRijksregisternummer_Click(object sender, RoutedEventArgs e)
        {
            Rijksregisternummer RNR = new Rijksregisternummer();
            lbRijksregisternummer.Content = RNR.IsValid(txtRijksregisternummer.Text).ToString();
        }
    }
}
